+++
title = "Discord & IRC"
aliases = [ "/community/irc/" ]
[menu.main]
    parent = "Community"
    name   = "Discord & IRC"
    weight = 15
+++
:linkattrs:

== Discord
Discord is a group chat platform originally intended for gamers but now popular for use for anything! 
Bringing modern chat features to the table, it is easy to use and cross platform (desktops and mobile)

image::discord.png[link="https://discord.gg/FANuKv8sZn"]


== IRC

IRC (Internet Relay Chat) is a group chat system that is popular in the open source community. KiCad has an IRC channel where you can discuss KiCad and related topics with other users like yourself.

To connect to our IRC channel, you can visit link:https://kiwiirc.com/nextclient/irc.libera.chat/kicad[Kiwi IRC]. Just choose a nickname for yourself, and press Start!

For those who wish to use a traditional IRC client and already know what this means, the connection details are:

[role="table table-striped table-condensed",cols="1h,1"]
|===
| Network | link:https://libera.chat/[Libera.Chat]
| Server  | irc.libera.chat
| Port    | 6697 (TLS)
| Channel | `#kicad`
|===
